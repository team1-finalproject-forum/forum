

# Project Description
**Forum** is an online discussion site where people can hold conversations by posting, commenting, and liking messages. 
# Functional Requirements
## Entities
- Each **user** have a username, password, email, and display name.
  - Username is unique and between 2 and 20 symbols.
  - Password is at least 8 symbols and should contain capital letter, digit and special symbol (+, -, \*, &, ^, …)
  - Display name is between 2 and 20 symbols.
  - Email is a valid email and unique in the system.
- Each **post** have a title, content, comments and likes.
## Public Part
The public part is accessible without authentication. 

On the home page anonymous users is presented with the core features of the platform as well as how many people are using it and how many posts have been created so far. 

Anonymous users can register and login.

Anonymous users can see a list of the top 10 most commented posts and a list of the 10 most recently created posts.
## Private part
Accessible only if the user is authenticated.

User can login and logout.

User can browse posts created by the other users. The list of posts support pagination.

User can view a single post including its title, content, comments, likes etc. 

User must be able to update their profile information including setting a profile picture. Users should not be able to change their username once registered.

User must be able to create a new post with at least a title and content.

Each user must be able to edit only personal posts or comments. 

Each user must be able to view all his posts (with option to filter and sort them), all his feedback and all feedback for any other user. List should support pagination.

Each user must be able to remove one or more of their own posts. Deleting a post should be available while reading the details of an individual post or when browsing the list of all posts.
## Administrative part
Accessible to users with administrative privileges.

Admin can search for a user by their username, email or display name. List with users support pagination.

Admin can block or unblock individual users. A blocked user can not create posts or comments. 

Our REST API provides the following capabilities:

1. Users
   1. CRUD operations 
   1. Block/unblock user 
   1. Search by username, email, or display name 
1. Posts
   1. CRUD operations 
   1. Comment 
   1. List and manage (comment/like) posts 
  
# Technical Requirements
## General
- We follow [OOP] principles when coding
- We follow [SOLID](https://en.wikipedia.org/wiki/SOLID), [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principles when coding
- We follow REST API design [best practices](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy) when designing the REST API
- We use layered structure for the project
- The service layer ("business" functionality) have at least 80% unit test code coverage
- Follow [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) when writing unit tests
## Database
The data of the application is stored in a relational database. Database structure avoid data duplication and empty data (normalized database).



