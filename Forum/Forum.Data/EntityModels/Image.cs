﻿using System.ComponentModel.DataAnnotations.Schema;


namespace Forum.Data.EntityModels
{
    public class Image
    {
        [ForeignKey("User")]
        public int ImageId { get; set; }
        public string ImageTitle { get; set; }
        public byte[] ImageData { get; set; }

        public virtual User User { get; set; }
    }
}
