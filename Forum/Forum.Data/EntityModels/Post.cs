﻿using Forum.Data.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class Post : Entity
    {
        [Key]
        public int PostId { get; set; }

        [Required]
        [MinLength(2), MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MinLength(2), MaxLength(500)]
        public string Content { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

        public ICollection<PostLike> PostLikes { get; set; } = new List<PostLike>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
    }
}
