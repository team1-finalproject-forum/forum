﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<UserRole> Users { get; set; } = new List<UserRole>();
    }
}
