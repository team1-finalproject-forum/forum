﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class PostLike
    {
        [Key]
        public int PostLikeId { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }
       
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
