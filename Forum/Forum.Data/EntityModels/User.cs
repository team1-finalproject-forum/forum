﻿using Forum.Data.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [MinLength(4), MaxLength(20)]
        public string UserName { get; set; }

        [Required]
        //[RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password must be at least 8 symbols should contain capital letter, digit and special symbol (+, -, *, &, ^, …)!")]
        public string Password { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email should be valid.")]
        public string Email { get; set; }

        [Required]
        [MinLength(4), MaxLength(20)]
        public string DisplayName { get; set; }

        public bool IsBlocked { get; set; }
        public virtual Image Image { get; set; }
        public ICollection<UserRole> Roles { get; set; } = new List<UserRole>();
        public ICollection<PostLike> PostLikes { get; set; } = new List<PostLike>();
        public ICollection<CommentLike> CommentLikes { get; set; } = new List<CommentLike>();
        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public ICollection<Post> Posts { get; set; } = new List<Post>();
    }
}
