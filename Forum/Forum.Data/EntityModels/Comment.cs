﻿using Forum.Data.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class Comment : Entity
    {
        [Key]
        public int CommentId { get; set; }

        [Required]
        [MinLength(2), MaxLength(500)]
        public string Description { get; set; }

        //[Required]
        public int PostId { get; set; }
        public Post Post { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        public ICollection<CommentLike> CommentLikes { get; set; } = new List<CommentLike>();
    }
}
