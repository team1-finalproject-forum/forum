﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class CommentLike
    {
        [Key]
        public int CommentLikeId { get; set; }

        // [Required]        
        public int CommentId { get; set; }

        public Comment Comment { get; set; }

        //[Required]      
        public int UserId { get; set; }

        public User User { get; set; }
    }
}
