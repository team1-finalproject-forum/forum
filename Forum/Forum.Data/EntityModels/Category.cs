﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Data.EntityModels
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();
    }
}
