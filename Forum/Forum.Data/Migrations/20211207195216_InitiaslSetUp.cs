﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Data.Migrations
{
    public partial class InitiaslSetUp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 1,
                column: "Name",
                value: "Suggestions");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 2,
                column: "Name",
                value: "Auto");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 3,
                column: "Name",
                value: "Pets");

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoryId", "Name" },
                values: new object[,]
                {
                    { 4, "Music" },
                    { 5, "Literature" },
                    { 6, "Cooking" }
                });

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 1,
                columns: new[] { "CommentId", "UserId" },
                values: new object[] { 2, 2 });

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 2,
                column: "UserId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 2,
                column: "Description",
                value: "Fabiq RS!");

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 2,
                column: "UserId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "Here you can give your suggestions how to develop this forum.", "Suggestions" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "What do you think about Skoda? I have one and I am very satisfied with it. Which model is you favourite?", "Anyone like skoda?" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "CategoryId", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Title", "UserId" },
                values: new object[] { 7, 3, "Let's take out our doggies together. Let's have a chit-chat while our dogs are doing their business!", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Doggers lets meet!!", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                columns: new[] { "DisplayName", "Email", "Password", "UserName" },
                values: new object[] { "Admin", "admin@mail.bg", "@dmin1234", "Admin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2,
                columns: new[] { "DisplayName", "Email", "Password" },
                values: new object[] { "IvanGrozni", "IvanGrozni@mail.bg", "ivan1+2+3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 3,
                columns: new[] { "Email", "Password" },
                values: new object[] { "vasil@mail.bg", "__vasil123" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "DisplayName", "Email", "IsBlocked", "Password", "UserName" },
                values: new object[,]
                {
                    { 9, "greyhound", "robot@mail.bg", false, "fen123!!$", "ADrian" },
                    { 8, "catlady", "kote@mail.bg", false, "mishel12$$#3", "Pisanata" },
                    { 7, "scienceguy", "emc2@mail.bg", false, "!nfinity123$#", "Einstein" },
                    { 6, "Mushmul", "robot@mail.bg", false, "!!@@fen123", "Achkata" },
                    { 5, "Mari", "Maria@mail.bg", false, "$$%maria123", "Maria" },
                    { 4, "Eli", "elisaveta@mail.bg", false, "el!saveta123", "Elisaveta" },
                    { 10, "Princes", "fairytale@mail.bg", false, "!ceLady85", "Ledenata" },
                    { 11, "maimuniaka", "admin@apeish.bg", false, "GOrilla123!", "Medi" }
                });

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 3,
                columns: new[] { "CommentId", "UserId" },
                values: new object[] { 2, 4 });

            migrationBuilder.InsertData(
                table: "CommentLikes",
                columns: new[] { "CommentLikeId", "CommentId", "UserId" },
                values: new object[,]
                {
                    { 4, 2, 5 },
                    { 5, 2, 6 },
                    { 6, 3, 9 },
                    { 7, 3, 10 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 1,
                columns: new[] { "Description", "PostId", "UserId" },
                values: new object[] { "I like skoda karoq. Really spacy car", 2, 4 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 3,
                columns: new[] { "Description", "UserId" },
                values: new object[] { "I like monkeys. I think I will make new post about this", 11 });

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 1,
                columns: new[] { "PostId", "UserId" },
                values: new object[] { 2, 8 });

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 3,
                columns: new[] { "PostId", "UserId" },
                values: new object[] { 2, 4 });

            migrationBuilder.InsertData(
                table: "PostLikes",
                columns: new[] { "PostLikeId", "PostId", "UserId" },
                values: new object[,]
                {
                    { 4, 3, 5 },
                    { 5, 3, 6 },
                    { 6, 3, 7 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 3,
                columns: new[] { "Content", "Title", "UserId" },
                values: new object[] { "Do you like cats and do you have one? I have persian. It makes grumpy faces all the time.", "Any cat lovers around?", 8 });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "PostId", "CategoryId", "Content", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Title", "UserId" },
                values: new object[,]
                {
                    { 5, 5, "I open this post for all fans of Paulo Coelho", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Paulo Coelho", 4 },
                    { 4, 4, "I love any kind of metal: trash, death, heavy etc. Hope there are like minded ppl to go on concerts with.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Metal for life!!!!!", 6 },
                    { 6, 6, "Pls help. How to cook musaka?! My husband always want some exotic meals. Always demand for kebapche(what in the hell is this) or shopska. Any housewives around to help me?", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Help!!", 10 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "CommentId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "ModifiedOn", "PostId", "UserId" },
                values: new object[,]
                {
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "If you read this, don't read it : Paulo Coelho :D", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 3 },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Metallica rulz :)", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, 9 },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "O come on. Those are lame. ppl know only them but there are so many out there.", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, 7 },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Es ist nicht schwer. Du brauchst etwas Hackfleisch und Kartofeln. Zwiebeln und Paprika auch", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, 3 },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "What did u say?!", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, 10 }
                });

            migrationBuilder.InsertData(
                table: "PostLikes",
                columns: new[] { "PostLikeId", "PostId", "UserId" },
                values: new object[,]
                {
                    { 7, 4, 11 },
                    { 8, 4, 9 },
                    { 9, 4, 6 },
                    { 10, 4, 4 }
                });

            migrationBuilder.InsertData(
                table: "CommentLikes",
                columns: new[] { "CommentLikeId", "CommentId", "UserId" },
                values: new object[,]
                {
                    { 9, 6, 2 },
                    { 10, 6, 3 },
                    { 11, 6, 4 },
                    { 12, 6, 5 },
                    { 13, 6, 6 },
                    { 8, 4, 7 },
                    { 14, 7, 11 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 10);

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 1,
                column: "Name",
                value: "Auto");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 2,
                column: "Name",
                value: "Health");

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryId",
                keyValue: 3,
                column: "Name",
                value: "Holiday");

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 1,
                columns: new[] { "CommentId", "UserId" },
                values: new object[] { 1, 1 });

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 2,
                column: "UserId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "CommentLikes",
                keyColumn: "CommentLikeId",
                keyValue: 3,
                columns: new[] { "CommentId", "UserId" },
                values: new object[] { 3, 3 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 1,
                columns: new[] { "Description", "PostId", "UserId" },
                values: new object[] { "Ok", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 2,
                column: "Description",
                value: "setting some stuff");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "CommentId",
                keyValue: 3,
                columns: new[] { "Description", "UserId" },
                values: new object[] { "hope you like it", 3 });

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 1,
                columns: new[] { "PostId", "UserId" },
                values: new object[] { 1, 1 });

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 2,
                column: "UserId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "PostLikes",
                keyColumn: "PostLikeId",
                keyValue: 3,
                columns: new[] { "PostId", "UserId" },
                values: new object[] { 3, 3 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 1,
                columns: new[] { "Content", "Title" },
                values: new object[] { "What is your opinion", "Testing" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 2,
                columns: new[] { "Content", "Title" },
                values: new object[] { "What do you think about", "More testing" });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "PostId",
                keyValue: 3,
                columns: new[] { "Content", "Title", "UserId" },
                values: new object[] { "Do you mind", "Never ending testing", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                columns: new[] { "DisplayName", "Email", "Password", "UserName" },
                values: new object[] { "Gosho", "Gosho@mail.bg", "Gosho123", "Gosho" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2,
                columns: new[] { "DisplayName", "Email", "Password" },
                values: new object[] { "Ivan", "Ivan@mail.bg", "Ivan123" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 3,
                columns: new[] { "Email", "Password" },
                values: new object[] { "Vasil@mail.bg", "Vasil123" });
        }
    }
}
