﻿using Forum.Data.EntityModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Forum.Data
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            var users = new List<User>
            {
                new User
                {
                    UserId = 1,
                    UserName = "Admin",
                    DisplayName = "Admin",
                    Email = "admin@mail.bg",
                    Password = "@dmin1234"
                },
                new User
                {
                    UserId = 2,
                    UserName = "Ivan",
                    DisplayName = "IvanGrozni",
                    Email = "IvanGrozni@mail.bg",
                    Password = "ivan1+2+3",

                },
                new User
                {
                    UserId = 3,
                    UserName = "Vasil",
                    DisplayName = "Vasil",
                    Email = "vasil@mail.bg",
                    Password = "__vasil123"
                },
                new User
                {
                    UserId = 4,
                    UserName = "Elisaveta",
                    DisplayName = "Eli",
                    Email = "elisaveta@mail.bg",
                    Password = "el!saveta123"
                },
                new User
                {
                    UserId = 5,
                    UserName = "Maria",
                    DisplayName = "Mari",
                    Email = "Maria@mail.bg",
                    Password = "$$%maria123"
                },
                new User
                {
                    UserId = 6,
                    UserName = "Achkata",
                    DisplayName = "Mushmul",
                    Email = "robot@mail.bg",
                    Password = "!!@@fen123"
                },
                new User
                {
                    UserId = 7,
                    UserName = "Einstein",
                    DisplayName = "scienceguy",
                    Email = "emc2@mail.bg",
                    Password = "!nfinity123$#"
                },
                new User
                {
                    UserId = 8,
                    UserName = "Pisanata",
                    DisplayName = "catlady",
                    Email = "kote@mail.bg",
                    Password = "mishel12$$#3"
                },
                new User
                {
                    UserId = 9,
                    UserName = "ADrian",
                    DisplayName = "greyhound",
                    Email = "robot@mail.bg",
                    Password = "fen123!!$"
                },
                new User
                {
                    UserId = 10,
                    UserName = "Ledenata",
                    DisplayName = "Princes",
                    Email = "fairytale@mail.bg",
                    Password = "!ceLady85"
                },
                new User
                {
                    UserId = 11,
                    UserName = "Medi",
                    DisplayName = "maimuniaka",
                    Email = "admin@apeish.bg",
                    Password = "GOrilla123!"
                },
            };

            var categories = new List<Category>
            {
                new Category
                {
                    CategoryId = 1,
                    Name = "Suggestions"
                },
                new Category
                {
                    CategoryId = 2,
                    Name = "Auto"
                },
                new Category
                {
                    CategoryId = 3,
                    Name = "Pets"
                },
                new Category
                {
                    CategoryId = 4,
                    Name = "Music"
                },
                new Category
                {
                    CategoryId = 5,
                    Name = "Literature"
                },
                new Category
                {
                    CategoryId = 6,
                    Name = "Cooking"
                }
            };

            var posts = new List<Post>
            {
                new Post
                {
                    PostId = 1,
                    Title = "Suggestions",
                    CategoryId = 1,
                    Content = "Here you can give your suggestions how to develop this forum.",
                    UserId = 1,

                },
                new Post
                {
                    PostId = 2,
                    Title = "Anyone like skoda?",
                    CategoryId = 2,
                    Content = "What do you think about Skoda? I have one and I am very satisfied with it. Which model is you favourite?",
                    UserId = 2
                },
                new Post
                {
                    PostId = 3,
                    Title = "Any cat lovers around?",
                    CategoryId = 3,
                    Content = "Do you like cats and do you have one? I have persian. It makes grumpy faces all the time.",
                    UserId = 8
                },
                new Post
                {
                    PostId = 4,
                    Title = "Metal for life!!!!!",
                    CategoryId = 4,
                    Content = "I love any kind of metal: trash, death, heavy etc. Hope there are like minded ppl to go on concerts with.",
                    UserId = 6
                },
                new Post
                {
                    PostId = 5,
                    Title = "Paulo Coelho",
                    CategoryId = 5,
                    Content = "I open this post for all fans of Paulo Coelho",
                    UserId = 4
                },
                new Post
                {
                    PostId = 6,
                    Title = "Help!!",
                    CategoryId = 6,
                    Content = "Pls help. How to cook musaka?! My husband always want some exotic meals. Always demand for kebapche(what in the hell is this) or shopska. Any housewives around to help me?",
                    UserId = 10
                },
                new Post
                {
                    PostId = 7,
                    Title = "Doggers lets meet!!",
                    CategoryId = 3,
                    Content = "Let's take out our doggies together. Let's have a chit-chat while our dogs are doing their business!",
                    UserId = 3
                }
            };

            var comments = new List<Comment>
            {
                new Comment
                {
                    CommentId = 1,
                    Description = "I like skoda karoq. Really spacy car",
                    PostId = 2,
                    UserId =4
                },
                new Comment
                {
                    CommentId = 2,
                    Description = "Fabiq RS!",
                    PostId = 2,
                    UserId =2
                },
                new Comment
                {
                    CommentId = 3,
                    Description= "I like monkeys. I think I will make new post about this",
                    PostId = 3,
                    UserId = 11
                },
                new Comment
                {
                    CommentId = 4,
                    Description= "Metallica rulz :)",
                    PostId = 4,
                    UserId = 9
                },
                new Comment
                {
                    CommentId = 5,
                    Description= "O come on. Those are lame. ppl know only them but there are so many out there.",
                    PostId = 4,
                    UserId = 7
                },
                new Comment
                {
                    CommentId = 6,
                    Description= "If you read this, don't read it : Paulo Coelho :D",
                    PostId = 5,
                    UserId = 3
                },
                new Comment
                {
                    CommentId = 7,
                    Description= "Es ist nicht schwer. Du brauchst etwas Hackfleisch und Kartofeln. Zwiebeln und Paprika auch",
                    PostId = 6,
                    UserId = 3
                },
                new Comment
                {
                    CommentId = 8,
                    Description= "What did u say?!",
                    PostId = 6,
                    UserId = 10
                },
            };

            var commentsLikes = new List<CommentLike>
            {
                new CommentLike
                {
                    CommentLikeId = 1,
                    UserId = 2,
                    CommentId =2
                },
                new CommentLike
                {
                    CommentLikeId = 2,
                    UserId = 3,
                    CommentId =2
                },
                new CommentLike
                {
                    CommentLikeId = 3,
                    UserId = 4,
                    CommentId = 2
                },
                 new CommentLike
                {
                    CommentLikeId = 4,
                    UserId = 5,
                    CommentId = 2
                },
                  new CommentLike
                {
                    CommentLikeId = 5,
                    UserId = 6,
                    CommentId = 2
                }, new CommentLike
                {
                    CommentLikeId = 6,
                    UserId = 9,
                    CommentId = 3
                },
                new CommentLike
                {
                    CommentLikeId = 7,
                    UserId = 10,
                    CommentId = 3
                },
                new CommentLike
                {
                    CommentLikeId = 8,
                    UserId = 7,
                    CommentId = 4
                },
                new CommentLike
                {
                    CommentLikeId = 9,
                    UserId = 2,
                    CommentId = 6
                },
                new CommentLike
                {
                    CommentLikeId = 10,
                    UserId = 3,
                    CommentId = 6
                },
                new CommentLike
                {
                    CommentLikeId = 11,
                    UserId = 4,
                    CommentId = 6
                },
                new CommentLike
                {
                    CommentLikeId = 12,
                    UserId = 5,
                    CommentId = 6
                },
                new CommentLike
                {
                    CommentLikeId = 13,
                    UserId = 6,
                    CommentId = 6
                },
                new CommentLike
                {
                    CommentLikeId = 14,
                    UserId = 11,
                    CommentId = 7
                },
            };

            var postLikes = new List<PostLike>
            {
                new PostLike
                {
                    PostLikeId = 1,
                    PostId = 2,
                    UserId = 8
                },
                new PostLike
                {
                    PostLikeId = 2,
                    PostId = 2,
                    UserId = 3
                },
                new PostLike
                {
                    PostLikeId = 3,
                    PostId = 2,
                    UserId = 4
                },
                new PostLike
                {
                    PostLikeId = 4,
                    PostId = 3,
                    UserId = 5
                },
                new PostLike
                {
                    PostLikeId = 5,
                    PostId = 3,
                    UserId = 6
                },
                new PostLike
                {
                    PostLikeId = 6,
                    PostId = 3,
                    UserId = 7
                },
                new PostLike
                {
                    PostLikeId = 7,
                    PostId = 4,
                    UserId = 11
                },
                new PostLike
                {
                    PostLikeId = 8,
                    PostId = 4,
                    UserId = 9
                },
                new PostLike
                {
                    PostLikeId = 9,
                    PostId = 4,
                    UserId = 6
                },
                new PostLike
                {
                    PostLikeId = 10,
                    PostId = 4,
                    UserId = 4
                },
            };

            var roles = new List<Role>
            {
                new Role
                {
                    RoleId = 1,
                    Name = "admin"
                },
                new Role
                {
                    RoleId = 2,
                    Name = "user"
                }
            };

            var userRoles = new List<UserRole>
            {
                new UserRole
                {
                    UserId = 1,
                    RoleId = 1
                },
                new UserRole
                {
                    UserId = 2,
                    RoleId = 2
                },
                new UserRole
                {
                    UserId = 3,
                    RoleId = 2
                }
            };

            modelBuilder.Entity<Category>().HasData(categories);
            modelBuilder.Entity<Comment>().HasData(comments);
            modelBuilder.Entity<CommentLike>().HasData(commentsLikes);
            modelBuilder.Entity<Post>().HasData(posts);
            modelBuilder.Entity<PostLike>().HasData(postLikes);
            modelBuilder.Entity<Role>().HasData(roles);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<UserRole>().HasData(userRoles);

        }
    }
}
