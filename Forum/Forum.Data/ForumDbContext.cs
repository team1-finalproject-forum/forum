﻿using Forum.Data.EntityModels;
using Microsoft.EntityFrameworkCore;

namespace Forum.Data
{
    public class ForumDbContext : DbContext
    {
        public ForumDbContext(DbContextOptions<ForumDbContext> options)
           : base(options)
        {

        }
        public ForumDbContext()
        {

        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CommentLike> CommentLikes { get; set; }
        public virtual DbSet<PostLike> PostLikes { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Image> Images { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);



            modelBuilder.Entity<PostLike>()
                 .HasOne(x => x.User)
                 .WithMany(s => s.PostLikes)
                 .HasForeignKey(x => x.UserId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<PostLike>()
                 .HasOne(x => x.Post)
                 .WithMany(s => s.PostLikes)
                 .HasForeignKey(x => x.PostId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>()
                 .HasOne(x => x.User)
                 .WithMany(s => s.Posts)
                 .HasForeignKey(x => x.UserId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Comment>()
                 .HasOne(x => x.User)
                 .WithMany(s => s.Comments)
                 .HasForeignKey(x => x.UserId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentLike>()
                 .HasOne(x => x.User)
                 .WithMany(s => s.CommentLikes)
                 .HasForeignKey(x => x.UserId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentLike>()
                 .HasOne(x => x.Comment)
                 .WithMany(s => s.CommentLikes)
                 .HasForeignKey(x => x.CommentId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UserRole>()
           .HasKey(ur => new { ur.UserId, ur.RoleId });

            modelBuilder.Entity<UserRole>()
                 .HasOne(ur => ur.User)
                 .WithMany(ur => ur.Roles)
                 .HasForeignKey(ur => ur.UserId)
                 .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(ur => ur.Users)
                .HasForeignKey(ur => ur.RoleId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Comment>().HasQueryFilter(x => !x.IsDeleted);
            //TODO : PostLike and CommentLike can be deleted
            // modelBuilder.Entity<PostLike>().HasQueryFilter(x => !x.IsDeleted);
           // modelBuilder.Entity<CommentLike>().HasQueryFilter(x => !x.IsDeleted);

            modelBuilder.Seed();
        }
    }
}
