﻿using System;

namespace Forum.Data.Abstracts
{
    public abstract class Entity
    {
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
