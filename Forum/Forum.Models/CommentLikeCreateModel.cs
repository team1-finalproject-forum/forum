﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class CommentLikeCreateModel
    {
        [Required]
        public int CommentId { get; set; }
        public int UserId { get; set; }
    }
}
