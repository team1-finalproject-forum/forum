﻿using Forum.Data.EntityModels;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class CategoryCreateModel
    {
        public int CategoryId { get; set; }
        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; }
    }
}