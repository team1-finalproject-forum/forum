﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class PostCreateModel
    {        
        [Required]
        [MinLength(2), MaxLength(50)]
        public string Title { get; set; }

        [MinLength(2), MaxLength(500)]
        public string Content { get; set; }

        [Required]                
        public int CategoryId { get; set; }  
        public int UserId { get; set; }
    }
}
