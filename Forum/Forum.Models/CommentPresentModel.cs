﻿using Forum.Data.EntityModels;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class CommentPresentModel
    {
        public int CommentId { get; set; }
        public string CommentedBy { get; set; }
        public string Description { get; set; }
       public int LikesCount { get; set; }
    }
}
