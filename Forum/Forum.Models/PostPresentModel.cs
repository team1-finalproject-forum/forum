﻿using Forum.Data.EntityModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class PostPresentModel
    {
        public int PostId { get; set; }
        public string CreatedBy { get; set; }
        public string Title { get; set; }

        [Required]
        [MinLength(2), MaxLength(500)]
        public string Content { get; set; }
        public string CategoryName { get; set; }
        public int CommentsCount { get; set; }
        public int LikesCount { get; set; }
        public ICollection<CommentPresentModel> Comments { get; set; } //= new List<CommentPresentModel>();
    }
}
