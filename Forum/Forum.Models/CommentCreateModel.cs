﻿using Forum.Data.EntityModels;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class CommentCreateModel
    {
        
        [Required]
        [MinLength(2), MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public int PostId { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}
