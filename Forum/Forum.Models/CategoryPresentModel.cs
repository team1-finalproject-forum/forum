﻿using Forum.Data.EntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class CategoryPresentModel
    {
        public int CategoryId { get; set; }
        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; }
    }

}
