﻿using Forum.Data.EntityModels;
using System.Linq;

namespace Forum.Models
{
    public static class ModelConvertExtensions
    {
        public static User ToModel(this UserCreateModel model)
        {
            return new User
            {
                UserName = model.UserName,
                DisplayName = model.DisplayName,
                Email = model.Email,
                Password = model.Password,
                IsBlocked = false
            };
        }
        public static UserPresentModel ToModel(this User model)
        {
            return new UserPresentModel
            {
                UserId = model.UserId,
                UserName = model.UserName,
                DisplayName = model.DisplayName,
                Email = model.Email,
                isBlocked = model.IsBlocked,
            };
        }

        public static PostPresentModel ToModel(this Post model)
        {
            return new PostPresentModel
            {
                PostId = model.PostId,
                CreatedBy = model.User.UserName,
                Title = model.Title,
                Content = model.Content,
                CategoryName = model.Category.Name,
                CommentsCount = model.Comments.Count,
                LikesCount = model.PostLikes.Count,
                Comments = model.Comments
                .Select(c => new CommentPresentModel()
                {
                    CommentId = c.CommentId,
                    CommentedBy = c.User.UserName,
                    Description = c.Description,
                    LikesCount = c.CommentLikes.Count
                })
                .ToList()
            };
        }

        public static Post ToModel(this PostCreateModel model)
        {
            return new Post
            {
                UserId = model.UserId,
                Title = model.Title,
                Content = model.Content,
                CategoryId = model.CategoryId
            };
        }
        public static Post ToModel(this PostCreateViewModel model)
        {
            return new Post
            {
                UserId = model.UserId,
                Title = model.Title,
                Content = model.Content,
                CategoryId = model.CategoryId
            };
        }
        public static PostCreateViewModel ToViewModel(this Post model)
        {
            return new PostCreateViewModel
            {
                UserId = model.UserId,
                Title = model.Title,
                Content = model.Content,
                CategoryId = model.CategoryId
            };
        }

        public static CommentPresentModel ToModel(this Comment model)
        {
            return new CommentPresentModel
            {
                CommentId = model.CommentId,
                CommentedBy = model.User.UserName,
                Description = model.Description,
                LikesCount = model.CommentLikes.Count
            };

        }

        public static Comment ToModel(this CommentCreateModel model)
        {
            return new Comment
            {
                Description = model.Description,
                PostId = model.PostId,
                UserId = model.UserId,
            };
        }

        public static PostLike ToModel(this LikeCreateModel model)
        {
            return new PostLike
            {
                UserId = model.UserId,
                PostId = model.EntityId
            };
        }

        public static CommentLike ToCommentLikeModel(this LikeCreateModel model)
        {
            return new CommentLike
            {
                UserId = model.UserId,
                CommentId = model.EntityId
            };
        }

        public static LikePresentModel ToCommentLikeModel(this CommentLike model)
        {
            return new LikePresentModel
            {
                LikedBy = model.User.DisplayName
            };
        }

        public static Category ToModel(this CategoryCreateModel model)
        {
            return new Category
            {
                CategoryId=model.CategoryId,
                Name = model.Name
            };
        }

        public static CategoryPresentModel ToModel(this Category model)
        {
            return new CategoryPresentModel
            {
                CategoryId = model.CategoryId,
                Name = model.Name
            };
        }

        public static PostLike ToModel(this PostLikeCreateModel model)
        {
            return new PostLike
            {
                PostId = model.PostId,
                UserId = model.UserId
            };
        }

        public static PostLikePresentModel ToModel(this PostLike model)
        {
            return new PostLikePresentModel
            {
                PostLikeBy = model.User.DisplayName
            };
        }

        public static CommentLikePresentModel ToModel(this CommentLike model)
        {
            return new CommentLikePresentModel
            {
                CommentLikeBy = model.User.DisplayName
            };
        }

        public static CommentLike ToModel(this CommentLikeCreateModel model)
        {
            return new CommentLike
            {
                UserId = model.UserId,
                CommentId = model.CommentId
            };
        }

        public static Image ToModel(this ImageModel model)
        {
            return new Image
            {
                ImageData = model.ImageData,
                ImageId = model.ImageId,
                ImageTitle = model.ImageTitle,
                User = model.User
            };
        }
        public static ImageModel ToModel(this Image model)
        {
            return new ImageModel
            {
                ImageData = model.ImageData,
                ImageId = model.ImageId,
                ImageTitle = model.ImageTitle,
                User = model.User
            };
        }
    }
}




