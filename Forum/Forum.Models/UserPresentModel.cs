﻿using Forum.Data.EntityModels;
using System;

namespace Forum.Models
{
    public class UserPresentModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool isBlocked { get; set; }
    }
}
