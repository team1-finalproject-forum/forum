﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class PostLikeCreateModel
    {          
        [Required]
        public int PostId { get; set; }
        public int UserId { get; set; }
    }
}
