﻿
using System.Collections.Generic;

namespace Forum.Models
{
    public class HomeInfoModel
    {
        public int UsersCount { get; set; }
        public int PostsCount { get; set; }

        public IList<PostPresentModel> Posts { get; set; }
    }
}
