﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class UserCreateModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "The name must be between {1} and {2}.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "The name must be between {1} and {2}.")]
        public string DisplayName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email should be valid.")]
        public string Email { get; set; }

        [Required]
        //[RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password must be at least 8 symbols should contain capital letter, digit and special symbol (+, -, *, &, ^, …)!")]
        public string Password { get; set; }
        [Required]
        //[RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password must be at least 8 symbols should contain capital letter, digit and special symbol (+, -, *, &, ^, …)!")]
        public string ConfirmPassword { get; set; }
    }
}
