﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Models
{
    public class UserLoginModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Email should be valid.")]
        public string Email { get; set; }
        public string  Password { get; set; }
    }
}
