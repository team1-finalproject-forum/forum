﻿using Forum.Data.EntityModels;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using System.Security.Authentication;

namespace Forum.Api.Helper

{
    public class AuthHelper:IAuthHelper
    {
        private readonly IUserService userService;
        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public User TryGetUser(string email)
        {
            try
            {
                return this.userService.GetByEmail(email);
            }
            catch (EntityNotFoundException)
            {
                throw new AuthenticationException("Invalid email.");
            }
        }
    }
}
