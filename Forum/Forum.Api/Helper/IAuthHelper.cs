﻿using Forum.Data.EntityModels;

namespace Forum.Api.Helper
{
    public interface IAuthHelper
    {
        User TryGetUser(string email);
    }
}
