﻿using Forum.Api.Exeptions;
using Forum.Api.Helper;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;


namespace Forum.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService commentService;
        private readonly IAuthHelper authHelper;

        public CommentsController(ICommentService commentService, IAuthHelper authHelper)
        {
            this.commentService = commentService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all comments
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetAllPost()
        {
            var comments = this.commentService.GetAllComments();
            return Ok(comments);
        }

        /// <summary>
        /// Get all comments in certain post
        /// </summary>
        /// <param name="id">Id of the post</param>
        /// <returns></returns>
        [HttpGet("post/{id}")]
        public IActionResult GetAllByPost(int id)
        {
            var post = this.commentService.GetAllByPost(id);
            return Ok(post);
        }

        /// <summary>
        /// Create new comment
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="commentCreateModel">Parameters for new comment</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Create([FromHeader] string credentials, [FromBody] CommentCreateModel commentCreateModel)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }

            var result = this.commentService.Create(commentCreateModel);

            return this.Created("post", result);
        }

        /// <summary>
        /// Edit a comment
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="id">Id of the comment to be edited</param>
        /// <param name="commentCreateModel">Parameters for the edited comment</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update([FromHeader] string credentials, int id, [FromBody] CommentCreateModel commentCreateModel)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);

                var result = this.commentService.Update(id, commentCreateModel);

                return this.Ok(result);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Remove a comment
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="id">Id of the comment to be removed</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);

                this.commentService.Delete(id);
                return this.NoContent();
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
