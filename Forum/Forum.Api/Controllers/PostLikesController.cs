﻿using Forum.Models;
using Forum.Services;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController, Route("[controller]")]
    public class PostLikesController : Controller
    {
        private readonly IPostLikeService postLikeService;

        public PostLikesController(IPostLikeService postLikeService)
        {
            this.postLikeService = postLikeService;
        }

        /// <summary>
        /// Create new CommentLike
        /// </summary>
        /// <param name="postLikeCreateModel">parameters for new CommentLike</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Create([FromBody] PostLikeCreateModel postLikeCreateModel)
        {
            var result = this.postLikeService.Create(postLikeCreateModel);

            return this.Created("postLike", result);
        }
        /// <summary>
        /// Get all postLikes
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetAllPostLikes()
        {
            var result = this.postLikeService.GetAll();

            return Ok(result);
        }

        /// <summary>
        /// Update postLike
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("")]
        public IActionResult Update([FromHeader] int id, [FromBody] PostLikeCreateModel model)
        {
            var result = this.postLikeService.Update(id, model);

            return Ok(result);
        }

        /// <summary>
        /// Remove a CommentLike
        /// </summary>
        /// <param name="id">Id of the CommentLike to be removed</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] int id)
        {
            try
            {
                this.postLikeService.Delete(id);
                return this.NoContent();
            }

            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
