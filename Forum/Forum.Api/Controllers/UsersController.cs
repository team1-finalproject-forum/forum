﻿using Forum.Api.Helper;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Forum.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IAuthHelper authHelper;

        public UsersController(IAuthHelper authHelper, IUserService userService)
        {
            this.authHelper = authHelper;
            this.userService = userService;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAll([FromHeader] string credentials)
        {

            if (!IsAdmin())
            {
                return this.Unauthorized();
            }
            var users = this.userService.GetAll();
            return this.Ok(users);
        }

        /// <summary>
        /// Get a user by Id
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="id">Id of the user to be ge</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetById([FromHeader] string credentials, int id)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }
            try
            {
                var user = this.userService.Get(id);
                return this.Ok(user);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="userCreateModel">parameters for creating a new user</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromHeader] string credentials, [FromBody] UserCreateModel userCreateModel)
        {

            if (userCreateModel == null)
            {
                return this.BadRequest();
            }

            try
            {
                var result = this.userService.Create(userCreateModel);
                return this.Created("post", result);
            }
            catch
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Edit a user
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="userCreateModel">New parameters for user</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromHeader] string credentials, [FromBody] UserCreateModel userCreateModel)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }

            if (id < 1 || userCreateModel == null)
            {
                return this.BadRequest();
            }
            try
            {
                var result = this.userService.Update(id, userCreateModel);
                return this.Ok(result);
            }
            catch
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="credentials">autorization parameter</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string credentials)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }

            if (id < 1)
            {
                return this.BadRequest();
            }

            this.userService.Delete(id);
            return this.NoContent();
        }

        /// <summary>
        /// Search for user
        /// </summary>
        /// <param name="userName">search by username</param>
        /// <param name="displayName">search by displayName</param>
        /// <param name="email">search by email</param>
        /// <param name="credentials">autorization parameter</param>
        /// <returns></returns>
        [HttpGet("search")]
        public IActionResult SearchUser(string userName, string displayName, string email, [FromHeader] string credentials)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }
            try
            {
                var result = this.userService.SearchUser(userName, displayName, email);
                return this.Ok(result);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        private bool IsAdmin()
        {
            var email = this.Request.Headers["credentials"];
            var user = this.authHelper.TryGetUser(email);
            return user.Roles.Any(r => r.Role.Name == "admin");
        }

    }
}
