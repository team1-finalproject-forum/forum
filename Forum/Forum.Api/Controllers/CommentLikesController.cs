﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController, Route("[controller]")]
    public class CommentLikesController : Controller
    {

        private readonly ICommentLikeService commentLikeService;

        public CommentLikesController(ICommentLikeService commentLikeService)
        {
            this.commentLikeService = commentLikeService;
        }

        /// <summary>
        /// Create a CommentLike
        /// </summary>
        /// <param name="commentLikeCreateModel">Parameters for new PostLike</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Create([FromBody] CommentLikeCreateModel commentLikeCreateModel)
        {
            var result = this.commentLikeService.Create(commentLikeCreateModel);

            return this.Created("commentLike", result);
        }

        /// <summary>
        /// Get all commentLikes
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetAllCommentLikes()
        {
            var result = this.commentLikeService.GetAll();

            return Ok(result);
        }

        /// <summary>
        /// Update commentLike
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("")]
        public IActionResult Update([FromHeader] int id, [FromBody] CommentLikeCreateModel model)
        {
            var result = this.commentLikeService.Update(id, model);

            return Ok(result);
        }

        /// <summary>
        /// Remove a CommentLike
        /// </summary>
        /// <param name="id">Id of the PostLike to be removed</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] int id)
        {
            try
            {
                this.commentLikeService.Delete(id);
                return this.NoContent();
            }

            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
