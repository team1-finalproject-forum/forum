﻿using Forum.Api.Exeptions;
using Forum.Api.Helper;
using Forum.Models;
using Forum.Services.Exceptions;
using Forum.Services.PostService;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Api.Controllers
{
    [ApiController, Route("[controller]")]

    public class PostsController : ControllerBase
    {
        private readonly IPostService postService;
        private readonly IAuthHelper authHelper;
        public PostsController(IPostService postService, IAuthHelper authHelper)
        {
            this.postService = postService;
            this.authHelper = authHelper;
        }

        
        // [HttpGet("")]
        // public IActionResult GetAllPost([FromQuery] PaginationCreateModel model)
        // {
        //     var post = this.postService.GetAllPosts(model);
        //     return Ok(post);
        // }

        /// <summary>
        /// Get all posts
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetAllPost()
        {
            var post = this.postService.GetAllPosts();
            return Ok(post);
        }

        /// <summary>
        /// Get a post by Id
        /// </summary>
        /// <param name="id">Id post</param>
        /// <returns></returns>
        [HttpGet("id")]
        public IActionResult GetById(int id)
        {
            var post = this.postService.GetById(id);
            return Ok(post);
        }

        /// <summary>
        /// Create new post
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="postCreateModel">Parameters for new post</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Create([FromHeader] string credentials, [FromBody] PostCreateModel postCreateModel)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }

            var result = this.postService.Create(postCreateModel);

            return this.Created("post", result);
        }

        /// <summary>
        /// Edit post
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="id">Id of the edited post</param>
        /// <param name="postCreateModel">parameters for edited post</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update([FromHeader] string credentials, int id, [FromBody] PostCreateModel postCreateModel)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);

                var result = this.postService.Update(id, postCreateModel);

                return this.Ok(result);
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Remove post
        /// </summary>
        /// <param name="credentials">autorization parameter</param>
        /// <param name="id">Id of the post to be removed</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                this.authHelper.TryGetUser(credentials);

                this.postService.Delete(id);
                return this.NoContent();
            }
            catch (AuthenticationException)
            {
                return this.Unauthorized();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
