﻿using Forum.Api.Helper;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Forum.Api.Controllers
{
    [ApiController, Route("[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        private readonly IAuthHelper authHelper;

        public CategoriesController(ICategoryService categoryService, IAuthHelper authHelper)
        {            
            this.categoryService = categoryService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetAll()
        {
            try
            {
                var category = this.categoryService.GetAll();

                return this.Ok(category);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }            
        }

        /// <summary>
        /// Get a category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var category = this.categoryService.GetById(id);

                return this.Ok(category);

            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Create a category
        /// </summary>
        /// <param name="credntials">autorization parameter</param>
        /// <param name="categoryCreateModel">parameters of new category</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Create([FromHeader] string credntials,[FromBody] CategoryCreateModel categoryCreateModel)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }

            try
            {
                var category = this.categoryService.Create(categoryCreateModel);

                return this.Created("created", category);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
        
        /// <summary>
        /// Edit existing category
        /// </summary>
        /// <param name="categoryCreateModel">new parameters for category</param>
        /// <param name="id">Id of the edited category</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update ([FromBody] CategoryCreateModel categoryCreateModel,int id)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }

            try
            {
                var category = this.categoryService.Update(id, categoryCreateModel);

                return this.Ok();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }           
        }

        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="id">Id of the category to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (!IsAdmin())
            {
                return this.Unauthorized();
            }

            //TODO IMPLEMENT SOFT DELETE 
            var category = this.categoryService.Delete(id);
            return this.Ok(category);
        }

        private bool IsAdmin()
        {
            var email = this.Request.Headers["credentials"];
            var user = this.authHelper.TryGetUser(email);
            return user.Roles.Any(r => r.Role.Name == "admin");
        }
    }
}
