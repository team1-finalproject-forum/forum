﻿using Forum.Models;
using Forum.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Forum.Tests
{
    [TestClass]
    public class CategoryTests
    {
        [TestMethod]
        public void GetByIdTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var service = new CategoryService(TestDbSet.mockDbContext.Object);

            var result = service.GetById(1);

            Assert.AreEqual("Test", result.Name);
        }

        [TestMethod]
        public void GetAllTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var service = new CategoryService(TestDbSet.mockDbContext.Object);

            var result = service.GetAll();

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void CreateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var service = new CategoryService(TestDbSet.mockDbContext.Object);
            var newCategory = new CategoryCreateModel
            {
                Name = "testCategory"
            };

            var result = service.Create(newCategory);

            Assert.AreEqual("testCategory", result.Name);
        }

        [TestMethod]
        public void DeleteTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var service = new CategoryService(TestDbSet.mockDbContext.Object);

            var result = service.Delete(1);

            Assert.AreEqual(true, result);

            var result1 = service.Delete(5);

            Assert.AreEqual(false, result1);
        }

        [TestMethod]
        public void UpdateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var service = new CategoryService(TestDbSet.mockDbContext.Object);
            var newCategory = new CategoryCreateModel
            {
                Name = "testCategory"
            };
            var result = service.Update(1, newCategory);

            Assert.AreEqual("testCategory", result.Name);
        }
    }
}
