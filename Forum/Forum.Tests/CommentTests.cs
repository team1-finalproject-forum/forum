﻿using Forum.Models;
using Forum.Services;
using Forum.Services.PostService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Forum.Tests
{
    [TestClass]
    public class CommentTests
    {
        [TestMethod]
        public void GetByIdModelTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetByIdModel(1);
            Assert.AreEqual(1, result.UserId);
        }

        [TestMethod]
        public void GetByIdTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetById(1);
            Assert.AreEqual(1, result.CommentId);
        }

        [TestMethod]
        public void GetAllTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetAllComments();
            Assert.AreEqual(2, result.Count());

            var result1 = service.GetAllComments(2, 0);
            Assert.AreEqual(2, result1.Count());
        }

        [TestMethod]
        public void GetAllByUserTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetAllByUser(1);
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void GetAllByPostTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetAllByPost(1);
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void CreateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var newComment = new CommentCreateModel
            {
                PostId = 1,
                UserId = 1,
                Description = "test"
            };

            var result = service.Create(newComment);
            Assert.AreEqual("test", result.Description);
        }

        [TestMethod]
        public void UpdateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var newComment = new CommentCreateModel
            {
                PostId = 1,
                UserId = 1,
                Description = "test"
            };

            var result = service.Update(1, newComment);
            Assert.AreEqual("test", result.Description);
        }

        [TestMethod]
        public void DeleteTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            
            var result = service.Delete(1);
            Assert.AreEqual(true, result);

            var result1 = service.Delete(5);
            Assert.AreEqual(false, result1);
        }
    }
}
