﻿using Forum.Models;
using Forum.Services;
using Forum.Services.Contracts;
using Forum.Services.PostService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Forum.Tests
{
    [TestClass]
    public class PostLikeTests
    {
        [TestMethod]
        public void GetByIdModelTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new PostLikeService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetByIdModel(1);

            Assert.AreEqual(1, result.PostId);
        }

        [TestMethod]
        public void GetAllTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new PostLikeService(TestDbSet.mockDbContext.Object, userService, postService);

            var result = service.GetAll();

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void CreateTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new PostLikeService(TestDbSet.mockDbContext.Object, userService, postService);

            var newPostLike = new PostLikeCreateModel
            {
                PostId = 1,
                UserId = 1,
            };
            var result = service.Create(newPostLike);

            Assert.AreEqual("test", result.PostLikeBy);

            var newPostLike1 = new PostLikeCreateModel
            {
                PostId = 1,
                UserId = 2,
            };
            var result1 = service.Create(newPostLike1);

            Assert.AreEqual("tester", result1.PostLikeBy);
        }

        [TestMethod]
        public void UpdateTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new PostLikeService(TestDbSet.mockDbContext.Object, userService, postService);

            var newPostLike = new PostLikeCreateModel
            {
                PostId = 1,
                UserId = 1,
            };
            var result = service.Update(1, newPostLike);

            Assert.AreEqual("test", result.PostLikeBy);
        }

        [TestMethod]
        public void DeleteTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var service = new PostLikeService(TestDbSet.mockDbContext.Object, userService, postService);
                        
            var result = service.Delete(1);

            Assert.AreEqual(true, result);

            var result1 = service.Delete(5);

            Assert.AreEqual(false, result1);
        }
    }
}
