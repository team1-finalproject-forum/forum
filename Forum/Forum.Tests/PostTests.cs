using Forum.Models;
using Forum.Services;
using Forum.Services.PostService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Forum.Tests
{
    [TestClass]
    public class PostTests
    {
        [TestMethod]
        public void GetByIdTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var result = service.GetById(1);

            Assert.AreEqual(1, result.PostId);
        }

        [TestMethod]
        public void GetByIdModelTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var result = service.GetByIdModel(1);

            Assert.AreEqual(1, result.PostId);
        }

        [TestMethod]
        public void GetTopCommentedPostTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var result = service.GetTopCommentedPost(1);

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void GetAllPostsTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);

            var result = service.GetAllPosts();
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void GetAllPostsPagedTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);

            var result = service.GetAllPosts(2, 1);
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void GetAllByUserTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);

            var result = service.GetAllByUser(1);
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void GetAllByCategoryTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);

            var result = service.GetAllByCategory(1);
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void CreateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var newData = new PostCreateModel
            {
                CategoryId = 1,
                UserId = 1,
                Content = "abcdefg",
                Title = "abcdefg"
            };
            var result = service.Create(newData);
            Assert.AreEqual("abcdefg", result.Title);
        }

        [TestMethod]
        public void UpdateTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var newData = new PostCreateModel
            {
                CategoryId = 1,
                UserId = 1,
                Content = "abcdefg",
                Title = "abcdefg"
            };
            var result = service.Update(1, newData);
            Assert.AreEqual("abcdefg", result.Title);
        }

        [TestMethod]      
        public void DeleteTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var newData = new PostCreateModel
            {
                CategoryId = 1,
                UserId = 1,
                Content = "abcdefg",
                Title = "abcdefg"
            };
            var result = service.Delete(1);
            Assert.AreEqual(true, result);
            var result1 = service.Delete(5);
            Assert.AreEqual(false, result1);
        }


        [TestMethod]
        public void PostsCountTest()
        {
            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var service = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
           
            var result = service.PostsCount();
            Assert.AreEqual(2, result);           
        }

    }
}
