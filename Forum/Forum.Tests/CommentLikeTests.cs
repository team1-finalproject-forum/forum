﻿using Forum.Models;
using Forum.Services;
using Forum.Services.PostService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Forum.Tests
{
    [TestClass]
    public class CommentLikeTests
    {
        [TestMethod]
        public void GetByIdModelTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var commentService = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var service = new CommentLikeService(TestDbSet.mockDbContext.Object, userService, commentService);

            var result = service.GetByIdModel(1);

            Assert.AreEqual(1, result.CommentId);
        }

        [TestMethod]
        public void GetAllTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var commentService = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var service = new CommentLikeService(TestDbSet.mockDbContext.Object, userService, commentService);

            var result = service.GetAll();

            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void CreateTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var commentService = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var service = new CommentLikeService(TestDbSet.mockDbContext.Object, userService, commentService);
            var newCommentLike = new CommentLikeCreateModel
            {
                CommentId = 1,
                UserId = 2
            };

            var result = service.Create(newCommentLike);

            Assert.AreEqual("tester", result.CommentLikeBy);
        }

        [TestMethod]
        public void UpdateTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var commentService = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var service = new CommentLikeService(TestDbSet.mockDbContext.Object, userService, commentService);
            var newCommentLike = new CommentLikeCreateModel
            {
                CommentId = 1,
                UserId = 1
            };

            var result = service.Update(1,newCommentLike);

            Assert.AreEqual("test", result.CommentLikeBy);
        }

        [TestMethod]
        public void DeleteTest()
        {

            TestDbSet.mockDbContext.Setup(db => db.Posts).Returns(TestDbSet.mockPostDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Users).Returns(TestDbSet.mockUserDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Categories).Returns(TestDbSet.mockCategoryDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Comments).Returns(TestDbSet.mockCommentDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.Roles).Returns(TestDbSet.mockRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.UserRoles).Returns(TestDbSet.mockUserRoleDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.PostLikes).Returns(TestDbSet.mockPostLikesDbSet.Object);
            TestDbSet.mockDbContext.Setup(db => db.CommentLikes).Returns(TestDbSet.mockCommentLikesDbSet.Object);

            var categoryService = new CategoryService(TestDbSet.mockDbContext.Object);
            var userService = new UserService(TestDbSet.mockDbContext.Object);
            var postService = new PostService(TestDbSet.mockDbContext.Object, categoryService, userService);
            var commentService = new CommentService(TestDbSet.mockDbContext.Object, userService, postService);
            var service = new CommentLikeService(TestDbSet.mockDbContext.Object, userService, commentService);
            var newCommentLike = new CommentLikeCreateModel
            {
                CommentId = 1,
                UserId = 1
            };

            var result = service.Delete(1);

            Assert.AreEqual(true, result);

            var result1 = service.Delete(5);

            Assert.AreEqual(false, result1);
        }
    }
}
