﻿using Forum.Data;
using Forum.Data.EntityModels;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Tests
{
    public static class TestDbSet
    {
        public static Mock<ForumDbContext> mockDbContext = new Mock<ForumDbContext>();
        public static List<Category> mockCategories = new List<Category>
            {
                new Category
                {
                    CategoryId = 1,
                    Name = "Test"
                }
            };

        public static List<Role> mockRoles = new List<Role>
            {
                new Role
                {
                      Name = "admin",
                      RoleId = 1
                },
                new Role
                {
                    Name = "user",
                    RoleId = 2
                }
            };

        public static List<User> mockUsers = new List<User>
            {
                new User
                {
                     UserId = 1,
                     UserName = "TestName",
                     Email = "test@mail.bg",
                     DisplayName = "test",
                     Password = "123Password!",
                },
                new User
                {
                     UserId = 2,
                     UserName = "TestNamer",
                     Email = "tester@mail.bg",
                     DisplayName = "tester",
                     Password = "123Password!",
                }
            };

        public static List<UserRole> mockUserRoles = new List<UserRole>
            {
                new UserRole
                {
                      RoleId = 1,
                      Role = mockRoles[0],
                      UserId = 1,
                      User = mockUsers[0]
                }
            };

        public static List<Post> mockPosts = new List<Post>
            {
                new Post
                {
                   PostId = 1,
                   CategoryId = 1,
                   Category = mockCategories[0],
                   UserId = 1,
                   User = mockUsers[0],
                   Title = "testtest",
                   Content = "testtest",
                   Comments = new List<Comment>
                   {
                        new Comment
                        {
                            CommentId = 1,
                            Description = "Ok",
                            PostId = 1,
                            UserId =1,
                            User = mockUsers[0]
                        },
                   }
                },
                new Post
                {
                    PostId = 1,
                    CategoryId = 1,
                    Category = mockCategories[0],
                    UserId = 2,
                    User = mockUsers[1],
                    Title = "Testtest",
                    Content = "Testtest",
                    Comments = new List<Comment>
                    {
                        new Comment
                        {
                           CommentId = 2,
                           Description = "setting some stuff",
                           PostId = 2,
                           UserId =2,
                           User = mockUsers[1],
                        },
                    }
                }
            };

        public static List<PostLike> mockPostLikes = new List<PostLike>
            {
                new PostLike
                {
                    PostLikeId = 1,
                    PostId = 1,
                    Post = mockPosts[0],
                    UserId = 1,
                    User = mockUsers[0],
                },
            };
        public static List<Comment> mockComments = new List<Comment>
            {
                new Comment
                {
                    CommentId = 1,
                    Description = "Ok",
                    PostId = 1,
                    UserId =1,
                    User = mockUsers[0],
                    CommentLikes = new List<CommentLike>
                    {
                        new CommentLike
                        {
                            CommentLikeId = 2,
                            UserId = 2,
                            User = mockUsers[1],
                            CommentId =2,
                        },
                    }
                },
                new Comment
                {
                    CommentId = 2,
                    Description = "setting some stuff",
                    PostId = 2,
                    UserId =2,
                    User = mockUsers[1],
                    CommentLikes = new List<CommentLike>
                    {
                         new CommentLike
                         {
                             CommentLikeId = 1,
                             UserId = 1,
                             User = mockUsers[0],
                             CommentId =1,
                         },
                    }
                },

            };
        public static List<CommentLike> mockCommentsLikes = new List<CommentLike>
            {
                new CommentLike
                {
                    CommentLikeId = 1,
                    UserId = 1,
                    User = mockUsers[0],
                    CommentId =1,
                    Comment = mockComments[0],
                },
                new CommentLike
                {
                    CommentLikeId = 2,
                    UserId = 2,
                    User = mockUsers[1],
                    CommentId =2,
                    Comment = mockComments[1]
                },
                
            };

        public static Mock<DbSet<Post>> mockPostDbSet = mockPosts.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<User>> mockUserDbSet = mockUsers.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<Category>> mockCategoryDbSet = mockCategories.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<Comment>> mockCommentDbSet = mockComments.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<Role>> mockRoleDbSet = mockRoles.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<UserRole>> mockUserRoleDbSet = mockUserRoles.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<PostLike>> mockPostLikesDbSet = mockPostLikes.AsQueryable().BuildMockDbSet();
        public static Mock<DbSet<CommentLike>> mockCommentLikesDbSet = mockCommentsLikes.AsQueryable().BuildMockDbSet();
    }
}
