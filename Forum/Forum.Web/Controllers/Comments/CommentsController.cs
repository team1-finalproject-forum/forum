﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.PostService;
using Forum.Web.Controllers.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Comments
{
    public class CommentsController : BaseController
    {
        private readonly IPostService postService;
        private readonly IUserService userService;
        private readonly ICommentService commentService;
        private readonly ICommentLikeService commentLikeService;

        public CommentsController(IPostService postService, IUserService userService, ICommentService commentService,ICommentLikeService commentLikeService)
            : base(userService)
        {
            this.postService = postService;
            this.commentService = commentService;
            this.commentLikeService = commentLikeService;
        }

        public IActionResult Index(int postId)
        {
            ViewBag.postId = postId;

            return View("Index");
        }

        [HttpPost]
        public IActionResult CreateCommentForPost([FromForm] int postId, string description)
        {
            //var userName = this.HttpContext.Session.GetString("CurrentUser");
            //var user = this.userService.GetByEmail(userName);
            var commentCreateModel = new CommentCreateModel();
            commentCreateModel.PostId = postId;
            commentCreateModel.UserId = CurrentUser.UserId;
            commentCreateModel.Description = description;
            this.commentService.Create(commentCreateModel);

            return RedirectToAction("CommentsForPost", "Posts", new { id = postId });
        }

        public IActionResult LikesForComment(int postId, int id)
        {
            //var userName = this.HttpContext.Session.GetString("CurrentUser");
            //var user = this.userService.GetByEmail(userName);
            var likeModel = new CommentLikeCreateModel();
            likeModel.CommentId = id;
            likeModel.UserId = CurrentUser.UserId;
            this.commentLikeService.Create(likeModel);

            return this.RedirectToAction("CommentsForPost","Posts", new { id = postId });
        }        
    }
}
