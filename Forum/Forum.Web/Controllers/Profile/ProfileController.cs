﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Web.Controllers.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Profile
{
    public class ProfileController : BaseController
    {
        public ProfileController(IUserService userService)
            : base(userService)
        {

        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UploadImage()
        {
            var file = Request.Form.Files[0];

            var img = new ImageModel();
            img.ImageTitle = file.FileName;
            img.User = CurrentUser;

            MemoryStream ms = new MemoryStream();
            file.CopyTo(ms);
            img.ImageData = ms.ToArray();

            ms.Close();
            ms.Dispose();

            this.userService.UploadPicture(img);

            ViewBag.Message = "Image(s) stored in database!";

            return View("Index");
        }

        [HttpPost]
        public ActionResult RetrieveImage()
        {
            var img = this.userService.GetPicture(CurrentUser.UserId);
            
            string imageBase64Data = Convert.ToBase64String(img.ImageData);
            string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

            ViewBag.ImageTitle = img.ImageTitle;
            ViewBag.ImageDataUrl = imageDataURL;

            return View("Index");
        }
    }
}
