﻿using Forum.Data.EntityModels;
using Forum.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Abstract
{
    public class BaseController : Controller
    {
        protected IUserService userService;
        private User user;
        public BaseController(IUserService userService)
        {
            this.userService = userService;
        }
        protected User CurrentUser
        {
            get
            {
                if (this.user == null)
                {
                    var userName = this.HttpContext.Session.GetString("CurrentUser");
                    this.user = this.userService.GetByEmail(userName);
                }

                return this.user;
            }
        }
    }
}
