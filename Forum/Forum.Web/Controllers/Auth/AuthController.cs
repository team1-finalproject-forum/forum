﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Forum.Web.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Authentication;

namespace Forum.Web.Controllers.Auth
{
    public class AuthController : Controller
    {
        private readonly IAuthHelper authHelper;
        private readonly IUserService userService;

        public AuthController(IAuthHelper authHelper, IUserService userService)
        {
            this.authHelper = authHelper;
            this.userService = userService;
        }
        [HttpGet]
        public IActionResult Register()
        {
            var userCreateModel = new UserCreateModel();

            return this.View(userCreateModel);
        }

        [HttpPost]
        public IActionResult Register([Bind("Email, UserName, DisplayName, Password, ConfirmPassword")] UserCreateModel userCreateModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(userCreateModel);
            }

            if (this.userService.Exists(userCreateModel.Email) )
            {
                this.ModelState.AddModelError("Username", "User with same email already exists.");
                return this.View(userCreateModel);
            }

            if (!userCreateModel.Password.Equals(userCreateModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(userCreateModel);
            }

            try
            {
                this.userService.Create(userCreateModel);
            }
            catch
            {
                //this.ModelState.AddModelError()
                return this.View(userCreateModel);
            }

            return this.RedirectToAction("Login");
        }
        //GET: /auth/login
        public IActionResult Login()
        {
            var userLoginModel = new UserLoginModel();

            return this.View(userLoginModel);
        }

        [HttpPost]
        public IActionResult Login([Bind("Email, Password")] UserLoginModel userLoginModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(userLoginModel);
            }

            try
            {
                var user = this.authHelper.TryGetUser(userLoginModel.Email, userLoginModel.Password);
                this.HttpContext.Session.SetString("CurrentUser", user.Email);
                this.HttpContext.Session.SetString("CurrentRoles", string.Join(',', user.Roles.Select(userRole => userRole.Role.Name)));

                return this.RedirectToAction("index", "home");
            }
            catch (AuthenticationException e)
            {
                this.ModelState.AddModelError("Email", e.Message);
                return this.View(userLoginModel);
            }
        }
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");

            return this.RedirectToAction("index", "home");
        }

    }
}
