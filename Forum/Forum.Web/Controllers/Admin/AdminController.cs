﻿using Forum.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Admin
{
    public class AdminController : Controller
    {
        private readonly IUserService userService;
        public AdminController(IUserService userService)
        {
            this.userService = userService;
        }
        public IActionResult Index()
        {
            var users = this.userService.GetAll();
            return View(users);
        }
        public IActionResult BlockUser(string email)
        {
            try
            {
                var user = this.userService.GetByEmail(email);
                this.userService.BlockUser(user.UserId, true);
            }
            catch (Exception)
            {

                throw;
            }
            return this.RedirectToAction("Index"); ;
        }
        public IActionResult UnblockUser(string email)
        {
            try
            {
                var user = this.userService.GetByEmail(email);
                this.userService.BlockUser(user.UserId, false);
            }
            catch (Exception)
            {

                throw;
            }
            return this.RedirectToAction("Index"); ;
        }
    }
}
