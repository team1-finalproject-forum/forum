﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Forum.Services.PostService;
using Forum.Web.Controllers.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Posts
{
    public class MyPostsController : BaseController
    {
        private readonly IPostService postService;
        private readonly ICategoryService categoryService;

        public MyPostsController(IPostService postService, IUserService userService, ICategoryService categoryService)
            : base(userService)
        {
            this.postService = postService;
            this.categoryService = categoryService;
        }

        public IActionResult Index()
        {
            var myPosts = this.postService.GetAllByUser(CurrentUser.UserId);
            return View(myPosts);
        }

        public IActionResult Create()
        {
            var post = new PostCreateViewModel();
            return this.PostView(post);
        }

        [HttpPost]
        public IActionResult Create(PostCreateViewModel post)
        {

            post.UserId = CurrentUser.UserId;
            post.Categories = this.GetCategories();

            if (!this.ModelState.IsValid)
            {
                return this.View(post);
            }

            this.postService.Create(post);
            return this.RedirectToAction("Index","Posts");
            
        }
        public IActionResult Edit(int id)
        {
            var post = this.postService.GetByIdModel(id);
            return this.PostView(post.ToViewModel());
        }
        [HttpPost]
        public IActionResult Edit (int id, PostCreateViewModel post)
        {
            if (!this.ModelState.IsValid)
            {
                return this.PostView(post);
            }
            try
            {      
                this.postService.Update(id, post);
            }
            catch (EntityNotFoundException)
            {
                return this.View(PostNotFound(id));
            }
            return this.RedirectToAction(nameof(this.Index));
        }
        private IActionResult PostView(PostCreateViewModel postViewModel)
        {
            postViewModel.Categories = this.GetCategories();
            return this.View(postViewModel);
        }

            private SelectList GetCategories()
        {
            var categories = this.categoryService.GetAll();
            return new SelectList(categories,"CategoryId","Name");
        }
        private IActionResult PostNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Post with id {id} does not exist.";
            return this.View("Error");
        }
    }
}
