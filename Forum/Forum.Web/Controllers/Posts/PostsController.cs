﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.PostService;
using Forum.Web.Controllers.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers
{
    public class PostsController : BaseController
    {
        private readonly IPostService postService;
        private readonly IPostLikeService postLikeService;
        private readonly ICommentLikeService commentLikeService;


        public PostsController(IPostService postService, IUserService userService, IPostLikeService postLikeService, ICommentLikeService commentLikeService)
       : base(userService)
        {
            this.postService = postService;
            this.postLikeService = postLikeService;
            this.commentLikeService = commentLikeService;
        }

        public IActionResult Index()
        {
            var posts = this.postService.GetAllPosts();
            return View(posts);
        }

        public IActionResult CommentsForPost(int id)
        {
            var post = this.postService.GetById(id);
            ViewBag.postId = id;

            return View(post);
        }

        public IActionResult LikesForPost(int id)
        {
            var likeModel = new PostLikeCreateModel
            {
                PostId = id,
                UserId = this.CurrentUser.UserId
            };

            this.postLikeService.Create(likeModel);

            return this.RedirectToAction("Index");
        }

       public IActionResult LikesForComment(int id)
       {
           var likeModel = new CommentLikeCreateModel();
           likeModel.CommentId = id;
           likeModel.UserId = CurrentUser.UserId;
           this.commentLikeService.Create(likeModel);
           return this.RedirectToAction("CommentsForPost", new { id = id });
       }
               
       public IActionResult AddCommentForPost(int postId, string description)
       {
           var commentModel = new CommentCreateModel();
           commentModel.PostId = postId;
           commentModel.UserId = CurrentUser.UserId;
           commentModel.Description = description;
           return this.RedirectToAction("CommentsForPost", new { id = postId });
       }
    }
}
