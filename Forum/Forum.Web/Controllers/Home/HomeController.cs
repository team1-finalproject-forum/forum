﻿using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.PostService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Controllers.Home
{
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly IPostService postService;

        public HomeController(IUserService userService, IPostService postService)
        {
            this.userService = userService;
            this.postService = postService;
        }
        public IActionResult Index()
        {
            var homeInfoModel = new HomeInfoModel();
            homeInfoModel.UsersCount = this.userService.UsersCount();
            homeInfoModel.PostsCount = this.postService.PostsCount();
            homeInfoModel.Posts = this.postService.GetTopCommentedPost(3).ToList();

            return View(homeInfoModel);
        }
    }
}
