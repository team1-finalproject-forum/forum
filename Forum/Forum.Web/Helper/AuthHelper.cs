﻿using Forum.Data.EntityModels;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace Forum.Web.Helper
{
    public class AuthHelper : IAuthHelper
    {
        private static readonly string ErrorMassage = "Invalid authentication info";
        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public User TryGetUser(string username)
        {
            try
            {
                return this.userService.GetByEmail(username);
            }
            catch (EntityNotFoundException)
            {
                throw new AuthenticationException(ErrorMassage);
}
        }
        public User TryGetUser(string username, string password)
        {
            var user = this.TryGetUser(username);
            if (user.Password != password)
            {
                throw new AuthenticationException(ErrorMassage);
            }
            return user;
        }

    }
}
