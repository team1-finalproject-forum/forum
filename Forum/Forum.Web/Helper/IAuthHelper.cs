﻿using Forum.Data.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Web.Helper
{
    public interface IAuthHelper
    {
        User TryGetUser(string username, string password);
    }
}
