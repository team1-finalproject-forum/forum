﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services
{
    public class CommentLikeService : ICommentLikeService
    {
        private readonly ForumDbContext dbContext;
        private readonly IUserService userService;
        private readonly ICommentService commentService;

        public CommentLikeService(ForumDbContext dbContex, IUserService userService, ICommentService commentService)
        {
            this.dbContext = dbContex;
            this.userService = userService;
            this.commentService = commentService;
        }

        public CommentLike GetByIdModel(int id)
        {
            var commentLike = this.dbContext.CommentLikes.FirstOrDefault(p => p.CommentLikeId == id);

            return commentLike; //?? throw new EntityNotFoundException();
        }

        public IEnumerable <CommentLikePresentModel> GetAll()
        {
            var commentLikes = this.dbContext.CommentLikes
                .Select(x => x.ToModel())
                .ToList();

            return commentLikes;
        }

        public CommentLikePresentModel Create(CommentLikeCreateModel createCommentLikeModel)
        {
            var commentLike = createCommentLikeModel.ToModel();
            var comment = this.commentService.GetByIdModel(commentLike.CommentId);
            var user = this.userService.GetModel(commentLike.UserId);

            commentLike.User = user;
            commentLike.Comment = comment;

            if (comment.UserId != commentLike.UserId && !this.dbContext.CommentLikes.Any(pl => pl.UserId == commentLike.UserId && pl.CommentId == commentLike.CommentId))
            {
                this.dbContext.CommentLikes.Add(commentLike);
                this.dbContext.SaveChanges();
            }
           
            return commentLike.ToModel();
        }

        public CommentLikePresentModel Update(int id, CommentLikeCreateModel model)
        {
            var commentLike = this.GetByIdModel(id);
            commentLike.CommentId = model.CommentId;
            commentLike.UserId = model.UserId;

            this.dbContext.SaveChanges();

            return commentLike.ToModel();
        }

        public bool Delete(int commentLikeId)
        {
            var commentLikeToDelete = this.GetByIdModel(commentLikeId);

            if (commentLikeToDelete == null)
            {
                return false;
            }
            //TODO check if user owns commentLikeToDelete???
            this.dbContext.CommentLikes.Remove(commentLikeToDelete);
            this.dbContext.SaveChanges();

            return true;
        }
    }
}
