﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services
{
    public class CategoryService : ICategoryService
    {
        private ForumDbContext dbContext;

        public CategoryService(ForumDbContext dbase)
        {
            this.dbContext = dbase;
        }
        public Category GetByIdModel(int id)
        {
            var result = this.dbContext.Categories.FirstOrDefault(x => x.CategoryId == id);
            return result ?? throw new EntityNotFoundException();
        }
        public CategoryPresentModel GetById(int id)
        {
            var result = this.dbContext.Categories.FirstOrDefault(x => x.CategoryId == id).ToModel();
            return result ?? throw new EntityNotFoundException();
        }
        public IEnumerable<CategoryPresentModel> GetAll()
        {
            var result = this.dbContext.Categories.ToList();
            return result.Select(x => x.ToModel());
        }

        public CategoryPresentModel Create(CategoryCreateModel model)
        {
            var result = model.ToModel();
            this.dbContext.Categories.Add(result);
            this.dbContext.SaveChanges();
            
            return result.ToModel();
        }

        public bool Delete(int categoryId)
        {
            var deleteCategory = this.dbContext.Categories.FirstOrDefault(x => x.CategoryId == categoryId);

            if (deleteCategory == null)
            {
                return false;
            }
            return true;
        }

        public CategoryPresentModel Update(int id,CategoryCreateModel model)
        {
            var categoryToUpdate = this.GetById(id);

            categoryToUpdate.Name = model.Name;

            this.dbContext.SaveChanges();

            return categoryToUpdate;
        }
   }
}
