﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Forum.Services.PostService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services
{

    public class CommentService : ServiceBase, ICommentService
    {
        private readonly ForumDbContext dbContext;
        private readonly IUserService userService;
        private readonly IPostService postService;
        public CommentService(ForumDbContext dbase, IUserService userService, IPostService postService)
        {
            this.dbContext = dbase;
            this.userService = userService;
            this.postService = postService;
        }
        private IQueryable<Comment> CommentsQuery
        {
            get
            {
                return this.dbContext.Comments
                                    .Include(p => p.User)
                                    .Include(p => p.CommentLikes);
            }
        }
        public Comment GetByIdModel(int id)
        {
            var model = this.CommentsQuery.FirstOrDefault(x => x.CommentId == id);

            return model; //?? throw new EntityNotFoundException();
        }

        public CommentPresentModel GetById(int id)
        {
            var model = this.CommentsQuery.FirstOrDefault(x => x.CommentId == id);

            return model.ToModel() ?? throw new EntityNotFoundException();
        }

        public IEnumerable<CommentPresentModel> GetAllComments()
        {

            return this.CommentsQuery.Select(x => x.ToModel()).ToList();
        }

        public IEnumerable<CommentPresentModel> GetAllComments(int pageSize, int pageNumber)
        {
            var comments = this.CommentsQuery.Select(p => p.ToModel()).Skip(pageNumber * (pageSize - 1)).Take(pageSize);

            return comments;
        }

        public IEnumerable<CommentPresentModel> GetAllByUser(int id)
        {

            return this.CommentsQuery.Where(x => x.UserId == id).Select(x => x.ToModel());
        }

        public IEnumerable<CommentPresentModel> GetAllByPost(int id)
        {

            return this.CommentsQuery.Where(x => x.PostId == id).Select(x => x.ToModel()); 
        }

        public CommentPresentModel Create(CommentCreateModel commentCreateModel)
        {
            var comment = commentCreateModel.ToModel();
            comment.User = this.userService.GetModel(comment.UserId);
            comment.Post = this.postService.GetByIdModel(comment.PostId);

            this.FillSystemData(comment);

            this.dbContext.Add(comment);
            this.dbContext.SaveChanges();

            return comment.ToModel();
        }

        public CommentPresentModel Update (int id, CommentCreateModel commentCreateModel)
        {
            var commentToUpdate = this.GetByIdModel(id);
            commentToUpdate.User = this.userService.GetModel(commentToUpdate.UserId);
            commentToUpdate.Post = this.postService.GetByIdModel(commentToUpdate.CommentId);

            commentToUpdate.Description = commentCreateModel.Description;
            commentToUpdate.ModifiedOn = DateTime.Now;

            this.dbContext.Comments.Add(commentToUpdate);
            this.dbContext.SaveChanges();

            return commentToUpdate.ToModel();
        }

        public bool Delete(int id)
        {
            var commentToDelete = this.GetByIdModel(id);

            if (commentToDelete == null)
            {
                return false;
            }

            commentToDelete.IsDeleted = true;
            commentToDelete.DeletedOn = DateTime.Now;

            this.dbContext.SaveChanges();

            return true;
        }       

    }
}
