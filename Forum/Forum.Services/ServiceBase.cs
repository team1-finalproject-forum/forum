﻿using Forum.Data.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Services
{
    public abstract class ServiceBase
    {
        public void FillSystemData(Entity entity)
        {
            entity.CreatedOn = DateTime.Now;
            entity.ModifiedOn = DateTime.Now;
            entity.IsDeleted = false;
            entity.DeletedOn = null;
        }
    }
}
