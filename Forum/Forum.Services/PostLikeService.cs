﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Forum.Services.PostService;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services.Contracts
{
    public class PostLikeService : IPostLikeService
    {
        private readonly ForumDbContext dbContext;
        private readonly IUserService userService;
        private readonly IPostService postService;

        public PostLikeService(ForumDbContext dbContex, IUserService userService, IPostService postService)
        {
            this.dbContext = dbContex;
            this.userService = userService;
            this.postService = postService;
        }

        public PostLike GetByIdModel(int id)
        {
            var postLike = this.dbContext.PostLikes.FirstOrDefault(p => p.PostLikeId == id);

            return postLike; //?? throw new EntityNotFoundException();
        }

        public IEnumerable<PostLikePresentModel> GetAll()
        {
            var postLikes = this.dbContext.PostLikes
                .Select(x => x.ToModel())
                .ToList();

            return postLikes;
        }

        public PostLikePresentModel Create(PostLikeCreateModel createPostLikeModel)
        {
            var postLike = createPostLikeModel.ToModel();
            var post = this.postService.GetByIdModel(postLike.PostId);
            var user = this.userService.GetModel(postLike.UserId);
            postLike.User = user;
            postLike.Post = post;

            if (post.UserId != postLike.UserId && !this.dbContext.PostLikes.Any(pl => pl.UserId == postLike.UserId && pl.PostId == postLike.PostId))
            {

                this.dbContext.PostLikes.Add(postLike);
                this.dbContext.SaveChanges();
            }
            
            return postLike.ToModel();
        }

        public PostLikePresentModel Update(int id, PostLikeCreateModel model)
        {
            var postLike = this.GetByIdModel(id);

            postLike.PostId = model.PostId;
            postLike.UserId = model.UserId;

            this.dbContext.SaveChanges();

            return postLike.ToModel();                
        }

        public bool Delete(int postLikeId)
        {
            var postLikeToDelete = this.GetByIdModel(postLikeId);

            if (postLikeToDelete == null)
            {
                return false;
            }
            //TODO check if user owns postliketodelete???
            this.dbContext.PostLikes.Remove(postLikeToDelete);
            this.dbContext.SaveChanges();

            return true;
        }                
    }
}
