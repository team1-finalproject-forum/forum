﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services.PostService
{
    public class PostService : ServiceBase, IPostService
    {
        private readonly ForumDbContext dbContext;
        private readonly ICategoryService categoryService;
        private readonly IUserService userService;

        public PostService(ForumDbContext dbContex, ICategoryService categoryService, IUserService userService)
        {
            this.dbContext = dbContex;
            this.categoryService = categoryService;
            this.userService = userService;
        }        

        private IQueryable<Post> PostsQuery
        {
            get
            {
                return this.dbContext.Posts
                                    .Include(p => p.User)
                                    .Include(p => p.PostLikes)
                                    .Include(p => p.Category)
                                    .Include(p => p.Comments)
                                            .ThenInclude(c => c.CommentLikes)
                                            .Include(x=>x.Comments)
                                            .ThenInclude(x=>x.User);

            }
        }

        public PostPresentModel GetById(int id)
        {
            var post = this.PostsQuery.FirstOrDefault(p => p.PostId == id);

            return post.ToModel() ?? throw new EntityNotFoundException();
        }

        public Post GetByIdModel(int id)
        {
            var post = this.PostsQuery.FirstOrDefault(p => p.PostId == id);

            return post; //?? throw new EntityNotFoundException();
        }

        public IEnumerable<PostPresentModel> GetTopCommentedPost(int count)
        {
            var result = this.PostsQuery
            .OrderByDescending(p => p.Comments.Count())
            .Take(count)
            .Select(p => p.ToModel());

            return result;
        }


        public IEnumerable<PostPresentModel> GetAllPosts(int pageSize, int pageNumber)
        {
            var posts = this.PostsQuery
                .Select(p => p.ToModel())
                .Skip(pageNumber * (pageSize - 1))
                .Take(pageSize);

            return posts;
        }

        public IEnumerable<PostPresentModel> GetAllPosts()
        {
            var posts = this.PostsQuery.Select(p => p.ToModel());

            return posts;
        }

        public IEnumerable<PostPresentModel> GetAllByUser(int userId)
        {
            var posts = this.PostsQuery
                .Where(x => x.UserId == userId)
                .Select(p => p.ToModel());

            return posts;
        }

        public IEnumerable<PostPresentModel> GetAllByCategory(int categoryId)
        {
            var posts = this.PostsQuery
                .Where(x => x.CategoryId == categoryId)
                .Select(x => x.ToModel());

            return posts;
        }

        public PostPresentModel Create(PostCreateModel createPostModel)
        {
            var post = createPostModel.ToModel();

            post.Category = this.categoryService.GetByIdModel(post.CategoryId);
            post.User = this.userService.GetModel(post.UserId);

            this.FillSystemData(post);

            this.dbContext.Posts.Add(post);
            this.dbContext.SaveChanges();

            return post.ToModel();
        }

        public PostPresentModel Update(int id, PostCreateModel model)
        {
            var postToUpdate = this.GetByIdModel(id);
            if (model.Title != null)
            {
                postToUpdate.Title = model.Title;
            }

            if (model.CategoryId != 0)
            {
                postToUpdate.CategoryId = model.CategoryId;
                postToUpdate.Category = this.categoryService.GetByIdModel(model.CategoryId);
            }

            if (model.Content != null)
            {
                postToUpdate.Content = model.Content;
            }

            postToUpdate.ModifiedOn = DateTime.Now;
            this.dbContext.SaveChanges();

            return postToUpdate.ToModel();
        }

        public bool Delete(int postId)
        {
            var postToDelete = this.GetByIdModel(postId);

            if (postToDelete == null)
            {
                return false;
            }

            postToDelete.IsDeleted = true;
            postToDelete.DeletedOn = DateTime.Now;
            this.dbContext.SaveChanges();

            return true;
        }

        public int PostsCount()
        {
            return this.dbContext.Posts.Count();
        }
    }
}
