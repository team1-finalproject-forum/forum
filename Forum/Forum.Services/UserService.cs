﻿using Forum.Data;
using Forum.Data.EntityModels;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Services
{
    public class UserService : IUserService
    {
        private readonly ForumDbContext dbContext;
        public UserService(ForumDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        private IQueryable<User> UserQuery
        {
            get
            {
                return this.dbContext.Users
                                    .Include(user => user.Roles)
                                        .ThenInclude(userRole => userRole.Role)
                                    .Include(user => user.Posts)
                                    .Include(user => user.PostLikes)
                                    .Include(user => user.CommentLikes);
            }
        }

        public UserPresentModel Create(UserCreateModel userCreateModel)
        {
            //ValidateEmail
            ValidateUser(userCreateModel);

            var user = userCreateModel.ToModel();

            var userRole = new UserRole
            {
                UserId = user.UserId,
                RoleId = 2
            };

            user.Roles.Add(userRole);

            this.dbContext.Users.Add(user);

            this.dbContext.SaveChanges();

            return user.ToModel();
        }

        public UserPresentModel Get(int id)
        {
            var user = this.UserQuery.FirstOrDefault(u => u.UserId == id);
            if (user != null)
            {
                return user.ToModel();
            }
            else
            {
                throw new EntityNotFoundException($"There is no user with id: {id}");
            }
        }

        public User GetModel(int id)
        {
            User user = this.UserQuery.FirstOrDefault(u => u.UserId == id);

            return user;// ?? throw new EntityNotFoundException();
        }

        public User GetByEmail(string email)
        {
            User user = this.UserQuery.FirstOrDefault(u => u.Email == email);

            return user ?? throw new EntityNotFoundException();
        }

        public User GetByUserName(string username)
        {
            User user = this.UserQuery.FirstOrDefault(u => u.UserName == username);

            return user ?? throw new EntityNotFoundException();
        }

        public User GetByDisplayName(string displayName)
        {
            User user = this.UserQuery.FirstOrDefault(u => u.DisplayName == displayName);

            return user ?? throw new EntityNotFoundException();
        }

        public IEnumerable<UserPresentModel> GetAll(int pageSize, int pageNumber)
        {
            var users = this.UserQuery.Select(p => p.ToModel()).Skip(pageNumber * (pageSize - 1)).Take(pageSize);

            return users;
        }

        public IEnumerable<UserPresentModel> GetAll()
        {
            var user = this.UserQuery.Select(u => u.ToModel());

            return user;
        }

        public UserPresentModel Update(int id, UserCreateModel user)
        {
            ValidateUser(user);

            var userToUpdate = this.GetModel(id);

            userToUpdate.UserName = user.UserName;
            userToUpdate.DisplayName = user.DisplayName;
            userToUpdate.Email = user.Email;
            userToUpdate.Password = user.Password;

            this.dbContext.SaveChanges();

            return userToUpdate.ToModel();
        }

        public bool Delete(int id)
        {
            var userToDelete = this.GetModel(id);

            if (userToDelete == null)
            {
                return false;
            }
            
            this.dbContext.Users.Remove(userToDelete);
            this.dbContext.SaveChanges();

            return true;
        }

        public void UploadPicture(ImageModel img)
        {
            //var result = this.dbContext.Images.FirstOrDefault(i => i.ImageId == img.ImageId);

            //if (result == null)
            //{
            //    throw new DuplicateEntityException();
            //}

            dbContext.Images.Add(img.ToModel());
            dbContext.SaveChanges();
        }

        public ImageModel GetPicture(int id)
        {
            var img = this.dbContext.Images.FirstOrDefault(i => i.ImageId == id);
            if (img == null)
            {
                throw new EntityNotFoundException();
            }
            return img.ToModel();
        }

        public UserPresentModel SearchUser(string userName, string displayName, string email)
        {
            if (userName != null)
            {
                return this.GetByUserName(userName).ToModel();
            }

            if (displayName != null)
            {
                return this.GetByDisplayName(displayName).ToModel();
            }
            if (email != null)
            {
                return this.GetByEmail(email).ToModel();
            }
            throw new EntityNotFoundException();
        }

        //TODO: Review again
        //public List<UserPresentModel> FilterUser(string searchPhrase)
        //{
        //    if (searchPhrase == null)
        //    {
        //        return GetAll().ToList();
        //    }
        //
        //    ////site 3 uslovi
        //    //var result = _dbContext.SomeTable.Where(x => DbFunctions.Like(x.NameAr, string.Format("%{0}%", searchStr))).FirstOrDefault();
        //
        //    List<User> resultList = new();
        //    resultList.Add(GetByUserName(searchPhrase));
        //    resultList.Add(GetByDisplayName(searchPhrase));
        //    resultList.Add(GetByEmail(searchPhrase));
        //
        //    return resultList.Select(r => r.ToModel()).ToList();
        //}

        public void BlockUser(int id, bool isBlocked)
        {
            var userToBeBlocked = this.GetModel(id);

            userToBeBlocked.IsBlocked = isBlocked;

            this.dbContext.SaveChanges();
        }

        public int UsersCount()
        {
            return this.dbContext.Users.Count();
        }

        private void ValidateUser(UserCreateModel user)
        {
            if (this.UserQuery.FirstOrDefault(u => u.UserName == user.UserName) != null)
            {
                throw new DuplicateEntityException($"There is already user with username {user.UserName}");
            }
            if (this.UserQuery.FirstOrDefault(u => u.Email == user.Email) != null)
            {
                throw new DuplicateEntityException($"There is already user with email {user.Email}");
            }
            if (!user.Password.Equals(user.ConfirmPassword))
            {
                throw new EntityNotFoundException("Confirm password should match password");
            }
        }
        public bool Exists(string email)
        {
            User user = this.UserQuery.FirstOrDefault(u => u.Email == email);

            return user !=null;
        }

    }
}
