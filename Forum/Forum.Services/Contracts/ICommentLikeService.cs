﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Contracts
{
    public interface ICommentLikeService
    {
        CommentLikePresentModel Create(CommentLikeCreateModel createCommentLikeModel);
        bool Delete(int commentLikeId);
        CommentLike GetByIdModel(int id);
        IEnumerable<CommentLikePresentModel> GetAll();
        CommentLikePresentModel Update(int id, CommentLikeCreateModel model);
    }
}