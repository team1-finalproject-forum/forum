﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.PostService
{
    public interface IPostService
    {
        PostPresentModel Create(PostCreateModel createPostModel);
        bool Delete(int postId);
        PostPresentModel Update(int id, PostCreateModel model);
        IEnumerable<PostPresentModel> GetAllByCategory(int categoryId);
        IEnumerable<PostPresentModel> GetAllByUser(int userId);       
        IEnumerable<PostPresentModel> GetAllPosts(int pageSize, int pageNumber);
        IEnumerable<PostPresentModel> GetAllPosts();
        PostPresentModel GetById(int id);
        Post GetByIdModel(int id);
        int PostsCount();
        IEnumerable<PostPresentModel> GetTopCommentedPost(int count);
    }
}