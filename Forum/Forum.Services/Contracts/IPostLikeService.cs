﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Contracts
{
    public interface IPostLikeService
    {
        PostLikePresentModel Create(PostLikeCreateModel createPostLikeModel);
        bool Delete(int postLikeId);
        PostLike GetByIdModel(int id);
        IEnumerable<PostLikePresentModel> GetAll();
        PostLikePresentModel Update(int id, PostLikeCreateModel model);
    }
}