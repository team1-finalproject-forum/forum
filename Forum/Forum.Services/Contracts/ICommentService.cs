﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Contracts
{
    public interface ICommentService
    {
        CommentPresentModel Create(CommentCreateModel model);
        bool Delete(int id);
        CommentPresentModel Update(int id, CommentCreateModel model);       
        IEnumerable<CommentPresentModel> GetAllComments(int pageSize, int pageNumber);
        IEnumerable<CommentPresentModel> GetAllComments();
        IEnumerable<CommentPresentModel> GetAllByPost(int id);
        IEnumerable<CommentPresentModel> GetAllByUser(int id);
        CommentPresentModel GetById(int id);
        public Comment GetByIdModel(int id);
    }
}