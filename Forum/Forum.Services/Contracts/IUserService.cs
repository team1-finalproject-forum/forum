﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Contracts
{
    public interface IUserService
    {
        User GetModel(int userId);
        UserPresentModel Get(int id);
        User GetByEmail(string username);
        IEnumerable<UserPresentModel> GetAll(int pageSize, int pageNumber);
        IEnumerable<UserPresentModel> GetAll();
        UserPresentModel Create(UserCreateModel userCreateModel);
        UserPresentModel Update(int id, UserCreateModel user);
        bool Delete(int id);
        //List<UserPresentModel> FilterUser(string searchPhrase);
        UserPresentModel SearchUser(string userName, string displayName, string email);
        void BlockUser(int id, bool isBlocked);
        int UsersCount();
        void UploadPicture(ImageModel img);
        ImageModel GetPicture(int id);
        bool Exists(string email);
    }
}
