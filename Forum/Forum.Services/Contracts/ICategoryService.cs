﻿using Forum.Data.EntityModels;
using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Contracts
{
    public interface ICategoryService
    {
       CategoryPresentModel Create(CategoryCreateModel model);
        bool Delete(int categoryId);
        CategoryPresentModel Update(int id, CategoryCreateModel model);
        IEnumerable<CategoryPresentModel> GetAll();
        CategoryPresentModel GetById(int id);
        Category GetByIdModel(int id);
    }
}